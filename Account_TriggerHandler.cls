public with sharing class Account_TriggerHandler extends TriggerHandler {
    
    public static String NAME = 'Account_TriggerHandler';
    
    private List<Account> toProcess;
    private Map<Id, Account> oldMap;
    private Map<Id, Account> newMap;
    
    public override String getHandlerName() {
        return NAME;
    }
    protected override void preparation() {
        this.setMaxLoopCount();
        this.toProcess = (Trigger.isInsert || Trigger.isUndelete || Trigger.isUpdate) ? Trigger.new : (Trigger.isDelete ? Trigger.old : new List<Account>());
        this.newMap = (Map<Id, Account>) ((!Trigger.isDelete) ? Trigger.newMap : new Map<Id, Account>());
        this.oldMap = (Map<Id, Account>)((Trigger.isUpdate || Trigger.isDelete) ? Trigger.oldMap: new Map<Id, Account>());
    }
    
    //for existed with id new record
    protected override void afterInsert() {
        createChildAccount(toProcess);
    }
    
    protected override void beforeUpdate() {
        List <Account> needCreateChild = new List <Account> ();
        //devide bulk accounts to parents and childs
        Map <Id, Account> parents = new Map <Id, Account> ();        
        for (Account item : toProcess) {
            if (item.isParent__c) {
                parents.put(item.Id, item);
            }
        }
        //get records with child sublist
        List <Account> existedChilds = [select id, name, isParent__c, (select id from ChildAccounts) from Account where Id in : parents.keySet()];
        for (Account acc : existedChilds) {
            if (acc.ChildAccounts.isEmpty()) {
                needCreateChild.add(parents.get(acc.Id));
            }
        }
        createChildAccount(needCreateChild);
    }
    
    // create child reocrds
    private void createChildAccount(List <Account> parents) {
        List <Account> childs = new List <Account> ();
        for(Account item : parents) {
            if (item.isParent__c) {
                childs.add(new Account(Name = 'child of '+ item.Name, ParentId = item.id, isParent__c = false));
            }
        }
        if (!childs.isEmpty())  {
            TriggerHandler.disableAllTriggerHandlers();
            insert childs;
            TriggerHandler.enableAllTriggerHandlers();
        }
    }
}