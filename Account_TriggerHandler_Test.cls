@IsTest
private class Account_TriggerHandler_Test {
    
    @TestSetup
    static void setup() {
        // Create  test accounts
        List<Account> testAccts = new List<Account>();
        for(Integer i = 0; i<2; i++) {
            testAccts.add(new Account(Name = 'TestAcct' + i, isParent__c = false));
        }
        insert testAccts;  
    }
    
    @IsTest
    static void checkChildUpdate() {        
        //given
        List <Account> accs = [SELECT Id, isParent__c FROM Account];        
        
        //when these account became parents
        for (Account acc : accs) {
            acc.isParent__c = true;
        }
        update accs;
        
        //then we have childs with parents
        List <Account> accsNew = [SELECT Id, isParent__c FROM Account];        
        System.assertEquals(accs.size()*2, accsNew.size());        
    }
    
    @IsTest
    static void checkChildCreate() {
        //given new parent accounts
     List <Account> testAccts = new List <Account>();        
        for(Integer i = 0; i < 2; i++) {
            testAccts.add(new Account(Name = 'TestAcct' + i, isParent__c = true));
        }
        insert testAccts;  
        
        Map <Id, Account> testAcctsMap = new Map <Id, Account>(testAccts);
        
        //then childs Accounts for these parents
        List <Account> accsNew = [SELECT Id, Name FROM Account WHERE
                                      Id NOT IN : testAcctsMap.keySet() 
                                      AND isParent__c = false 
                                      AND ParentId IN : testAcctsMap.keySet()];        
        System.assertNotEquals(0, accsNew.size());      
        
    }
}